
const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const decentralandLogo_01 = new Entity()
decentralandLogo_01.setParent(scene)
const gltfShape = new GLTFShape('models/DecentralandLogo_01/DecentralandLogo_01.glb')
decentralandLogo_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(1, 0, 30),
  rotation: new Quaternion(0.5000000000000001, -0.5000000000000002, -0.5, 0.5000000000000001),
  scale: new Vector3(1, 1, 1)
})
decentralandLogo_01.addComponentOrReplace(transform_2)
engine.addEntity(decentralandLogo_01)

const waterPatchCornerOutside_01 = new Entity()
waterPatchCornerOutside_01.setParent(scene)
const gltfShape_2 = new GLTFShape('models/WaterPatchCornerOutside_01/WaterPatchCornerOutside_01.glb')
waterPatchCornerOutside_01.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(24, 0, 24),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 1.6653345369377348e-16),
  scale: new Vector3(1, 1, 1)
})
waterPatchCornerOutside_01.addComponentOrReplace(transform_3)
engine.addEntity(waterPatchCornerOutside_01)

const waterPatchSide_01 = new Entity()
waterPatchSide_01.setParent(scene)
const gltfShape_3 = new GLTFShape('models/WaterPatchSide_01/WaterPatchSide_01.glb')
waterPatchSide_01.addComponentOrReplace(gltfShape_3)
const transform_4 = new Transform({
  position: new Vector3(20, 0, 23.5),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.914335439641036e-16),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01.addComponentOrReplace(transform_4)
engine.addEntity(waterPatchSide_01)

const waterPatchSide_01_2 = new Entity()
waterPatchSide_01_2.setParent(scene)
waterPatchSide_01_2.addComponentOrReplace(gltfShape_3)
const transform_5 = new Transform({
  position: new Vector3(24, 0, 16.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_2.addComponentOrReplace(transform_5)
engine.addEntity(waterPatchSide_01_2)

const waterPatchCurve_02 = new Entity()
waterPatchCurve_02.setParent(scene)
const gltfShape_4 = new GLTFShape('models/WaterPatchCurve_02/WaterPatchCurve_02.glb')
waterPatchCurve_02.addComponentOrReplace(gltfShape_4)
const transform_6 = new Transform({
  position: new Vector3(24, 0, 8.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_02.addComponentOrReplace(transform_6)
engine.addEntity(waterPatchCurve_02)

const waterPatchCurve_01 = new Entity()
waterPatchCurve_01.setParent(scene)
const gltfShape_5 = new GLTFShape('models/WaterPatchCurve_01/WaterPatchCurve_01.glb')
waterPatchCurve_01.addComponentOrReplace(gltfShape_5)
const transform_7 = new Transform({
  position: new Vector3(24.5, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_01.addComponentOrReplace(transform_7)
engine.addEntity(waterPatchCurve_01)

const waterPatchSide_01_3 = new Entity()
waterPatchSide_01_3.setParent(scene)
waterPatchSide_01_3.addComponentOrReplace(gltfShape_3)
const transform_8 = new Transform({
  position: new Vector3(24, 0, 24.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_3.addComponentOrReplace(transform_8)
engine.addEntity(waterPatchSide_01_3)

const waterPatchSide_01_4 = new Entity()
waterPatchSide_01_4.setParent(scene)
waterPatchSide_01_4.addComponentOrReplace(gltfShape_3)
const transform_9 = new Transform({
  position: new Vector3(3.5, 0, 24),
  rotation: new Quaternion(0, 1, 0, 9.71445146547012e-17),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_4.addComponentOrReplace(transform_9)
engine.addEntity(waterPatchSide_01_4)

const waterPatchCornerOutside_01_2 = new Entity()
waterPatchCornerOutside_01_2.setParent(scene)
waterPatchCornerOutside_01_2.addComponentOrReplace(gltfShape_2)
const transform_10 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
waterPatchCornerOutside_01_2.addComponentOrReplace(transform_10)
engine.addEntity(waterPatchCornerOutside_01_2)

const waterPatchSide_01_5 = new Entity()
waterPatchSide_01_5.setParent(scene)
waterPatchSide_01_5.addComponentOrReplace(gltfShape_3)
const transform_11 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_5.addComponentOrReplace(transform_11)
engine.addEntity(waterPatchSide_01_5)

const waterPatchSide_01_6 = new Entity()
waterPatchSide_01_6.setParent(scene)
waterPatchSide_01_6.addComponentOrReplace(gltfShape_3)
const transform_12 = new Transform({
  position: new Vector3(8, 0, 16),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
waterPatchSide_01_6.addComponentOrReplace(transform_12)
engine.addEntity(waterPatchSide_01_6)

const waterPatchCurve_02_2 = new Entity()
waterPatchCurve_02_2.setParent(scene)
waterPatchCurve_02_2.addComponentOrReplace(gltfShape_4)
const transform_13 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_02_2.addComponentOrReplace(transform_13)
engine.addEntity(waterPatchCurve_02_2)

const waterPatchCurve_01_2 = new Entity()
waterPatchCurve_01_2.setParent(scene)
waterPatchCurve_01_2.addComponentOrReplace(gltfShape_5)
const transform_14 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
waterPatchCurve_01_2.addComponentOrReplace(transform_14)
engine.addEntity(waterPatchCurve_01_2)

const floorFantasyRocks_04 = new Entity()
floorFantasyRocks_04.setParent(scene)
const gltfShape_6 = new GLTFShape('models/FloorFantasyRocks_04/FloorFantasyRocks_04.glb')
floorFantasyRocks_04.addComponentOrReplace(gltfShape_6)
const transform_15 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorFantasyRocks_04.addComponentOrReplace(transform_15)
engine.addEntity(floorFantasyRocks_04)

const floorFantasyRocks_04_2 = new Entity()
floorFantasyRocks_04_2.setParent(scene)
floorFantasyRocks_04_2.addComponentOrReplace(gltfShape_6)
const transform_16 = new Transform({
  position: new Vector3(24, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorFantasyRocks_04_2.addComponentOrReplace(transform_16)
engine.addEntity(floorFantasyRocks_04_2)

const floorFantasyRocks_04_3 = new Entity()
floorFantasyRocks_04_3.setParent(scene)
floorFantasyRocks_04_3.addComponentOrReplace(gltfShape_6)
const transform_17 = new Transform({
  position: new Vector3(8, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorFantasyRocks_04_3.addComponentOrReplace(transform_17)
engine.addEntity(floorFantasyRocks_04_3)

const floorFantasyRocks_04_4 = new Entity()
floorFantasyRocks_04_4.setParent(scene)
floorFantasyRocks_04_4.addComponentOrReplace(gltfShape_6)
const transform_18 = new Transform({
  position: new Vector3(24, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorFantasyRocks_04_4.addComponentOrReplace(transform_18)
engine.addEntity(floorFantasyRocks_04_4)

const tree_Dead_02 = new Entity()
tree_Dead_02.setParent(scene)
const gltfShape_7 = new GLTFShape('models/Tree_Dead_02/Tree_Dead_02.glb')
tree_Dead_02.addComponentOrReplace(gltfShape_7)
const transform_19 = new Transform({
  position: new Vector3(19, 0, 29.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_02.addComponentOrReplace(transform_19)
engine.addEntity(tree_Dead_02)

const tree_Dead_01 = new Entity()
tree_Dead_01.setParent(scene)
const gltfShape_8 = new GLTFShape('models/Tree_Dead_01/Tree_Dead_01.glb')
tree_Dead_01.addComponentOrReplace(gltfShape_8)
const transform_20 = new Transform({
  position: new Vector3(12.5, 5.551115123125783e-17, 28.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
tree_Dead_01.addComponentOrReplace(transform_20)
engine.addEntity(tree_Dead_01)

const stalagmite_03 = new Entity()
stalagmite_03.setParent(scene)
const gltfShape_9 = new GLTFShape('models/Stalagmite_03/Stalagmite_03.glb')
stalagmite_03.addComponentOrReplace(gltfShape_9)
const transform_21 = new Transform({
  position: new Vector3(19.140573396674746, 0, 7.907326576037022),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
stalagmite_03.addComponentOrReplace(transform_21)
engine.addEntity(stalagmite_03)

const stone_02 = new Entity()
stone_02.setParent(scene)
const gltfShape_10 = new GLTFShape('models/Stone_02/Stone_02.glb')
stone_02.addComponentOrReplace(gltfShape_10)
const transform_22 = new Transform({
  position: new Vector3(9, 0, 23.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
stone_02.addComponentOrReplace(transform_22)
engine.addEntity(stone_02)

const stalagmite_04 = new Entity()
stalagmite_04.setParent(scene)
const gltfShape_11 = new GLTFShape('models/Stalagmite_04/Stalagmite_04.glb')
stalagmite_04.addComponentOrReplace(gltfShape_11)
const transform_23 = new Transform({
  position: new Vector3(18.5, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
stalagmite_04.addComponentOrReplace(transform_23)
engine.addEntity(stalagmite_04)

const tree_01 = new Entity()
tree_01.setParent(scene)
const gltfShape_12 = new GLTFShape('models/Tree_01/Tree_01.glb')
tree_01.addComponentOrReplace(gltfShape_12)
const transform_24 = new Transform({
  position: new Vector3(30, 0, 3),
  rotation: new Quaternion(0, -1.0000000000000007, 0, 9.71445146547012e-17),
  scale: new Vector3(1, 1, 1)
})
tree_01.addComponentOrReplace(transform_24)
engine.addEntity(tree_01)

const stone_03 = new Entity()
stone_03.setParent(scene)
const gltfShape_13 = new GLTFShape('models/Stone_03/Stone_03.glb')
stone_03.addComponentOrReplace(gltfShape_13)
const transform_25 = new Transform({
  position: new Vector3(8.5, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
stone_03.addComponentOrReplace(transform_25)
engine.addEntity(stone_03)

const tree_02 = new Entity()
tree_02.setParent(scene)
const gltfShape_14 = new GLTFShape('models/Tree_02/Tree_02.glb')
tree_02.addComponentOrReplace(gltfShape_14)
const transform_26 = new Transform({
  position: new Vector3(2, 0, 2.5),
  rotation: new Quaternion(0, -0.7071067811865478, 0, 0.7071067811865477),
  scale: new Vector3(1, 1, 1)
})
tree_02.addComponentOrReplace(transform_26)
engine.addEntity(tree_02)

const bush_Fantasy_04 = new Entity()
bush_Fantasy_04.setParent(scene)
const gltfShape_15 = new GLTFShape('models/Bush_Fantasy_04/Bush_Fantasy_04.glb')
bush_Fantasy_04.addComponentOrReplace(gltfShape_15)
const transform_27 = new Transform({
  position: new Vector3(13.5, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
bush_Fantasy_04.addComponentOrReplace(transform_27)
engine.addEntity(bush_Fantasy_04)

const bush_Fantasy_01 = new Entity()
bush_Fantasy_01.setParent(scene)
const gltfShape_16 = new GLTFShape('models/Bush_Fantasy_01/Bush_Fantasy_01.glb')
bush_Fantasy_01.addComponentOrReplace(gltfShape_16)
const transform_28 = new Transform({
  position: new Vector3(12.5, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
bush_Fantasy_01.addComponentOrReplace(transform_28)
engine.addEntity(bush_Fantasy_01)

const bush_Fantasy_02 = new Entity()
bush_Fantasy_02.setParent(scene)
const gltfShape_17 = new GLTFShape('models/Bush_Fantasy_02/Bush_Fantasy_02.glb')
bush_Fantasy_02.addComponentOrReplace(gltfShape_17)
const transform_29 = new Transform({
  position: new Vector3(13.5, 0, 6.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
bush_Fantasy_02.addComponentOrReplace(transform_29)
engine.addEntity(bush_Fantasy_02)

const mushrooms_04 = new Entity()
mushrooms_04.setParent(scene)
const gltfShape_18 = new GLTFShape('models/Mushrooms_04/Mushrooms_04.glb')
mushrooms_04.addComponentOrReplace(gltfShape_18)
const transform_30 = new Transform({
  position: new Vector3(9.5, 0, 6.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
mushrooms_04.addComponentOrReplace(transform_30)
engine.addEntity(mushrooms_04)

const mushrooms_04_2 = new Entity()
mushrooms_04_2.setParent(scene)
mushrooms_04_2.addComponentOrReplace(gltfShape_18)
const transform_31 = new Transform({
  position: new Vector3(9, 0, 6.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
mushrooms_04_2.addComponentOrReplace(transform_31)
engine.addEntity(mushrooms_04_2)

const pond_Stone_01 = new Entity()
pond_Stone_01.setParent(scene)
const gltfShape_19 = new GLTFShape('models/Pond_Stone_01/Pond_Stone_01.glb')
pond_Stone_01.addComponentOrReplace(gltfShape_19)
const transform_32 = new Transform({
  position: new Vector3(13.5, 0, 26.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
pond_Stone_01.addComponentOrReplace(transform_32)
engine.addEntity(pond_Stone_01)

const rockFloor_Module_4M = new Entity()
rockFloor_Module_4M.setParent(scene)
const gltfShape_20 = new GLTFShape('models/RockFloor_Module_4M/RockFloor_Module_4M.glb')
rockFloor_Module_4M.addComponentOrReplace(gltfShape_20)
const transform_33 = new Transform({
  position: new Vector3(18, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M.addComponentOrReplace(transform_33)
engine.addEntity(rockFloor_Module_4M)

const rockFloor_Module_4M_2 = new Entity()
rockFloor_Module_4M_2.setParent(scene)
rockFloor_Module_4M_2.addComponentOrReplace(gltfShape_20)
const transform_34 = new Transform({
  position: new Vector3(18, 0, 4.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rockFloor_Module_4M_2.addComponentOrReplace(transform_34)
engine.addEntity(rockFloor_Module_4M_2)

const writenStone_01 = new Entity()
writenStone_01.setParent(scene)
const gltfShape_21 = new GLTFShape('models/WritenStone_01/WritenStone_01.glb')
writenStone_01.addComponentOrReplace(gltfShape_21)
const transform_35 = new Transform({
  position: new Vector3(19, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
writenStone_01.addComponentOrReplace(transform_35)
engine.addEntity(writenStone_01)

const archWay_02 = new Entity()
archWay_02.setParent(scene)
const gltfShape_22 = new GLTFShape('models/ArchWay_02/ArchWay_02.glb')
archWay_02.addComponentOrReplace(gltfShape_22)
const transform_36 = new Transform({
  position: new Vector3(16, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
archWay_02.addComponentOrReplace(transform_36)
engine.addEntity(archWay_02)

const log_Large_01 = new Entity()
log_Large_01.setParent(scene)
const gltfShape_23 = new GLTFShape('models/Log_Large_01/Log_Large_01.glb')
log_Large_01.addComponentOrReplace(gltfShape_23)
const transform_37 = new Transform({
  position: new Vector3(11.5, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
log_Large_01.addComponentOrReplace(transform_37)
engine.addEntity(log_Large_01)

const log_Large_01_2 = new Entity()
log_Large_01_2.setParent(scene)
log_Large_01_2.addComponentOrReplace(gltfShape_23)
const transform_38 = new Transform({
  position: new Vector3(22, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_2.addComponentOrReplace(transform_38)
engine.addEntity(log_Large_01_2)

const log_Large_01_3 = new Entity()
log_Large_01_3.setParent(scene)
log_Large_01_3.addComponentOrReplace(gltfShape_23)
const transform_39 = new Transform({
  position: new Vector3(22, 0, 23.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_3.addComponentOrReplace(transform_39)
engine.addEntity(log_Large_01_3)

const log_Large_01_4 = new Entity()
log_Large_01_4.setParent(scene)
log_Large_01_4.addComponentOrReplace(gltfShape_23)
const transform_40 = new Transform({
  position: new Vector3(24, 0, 10),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_4.addComponentOrReplace(transform_40)
engine.addEntity(log_Large_01_4)

const log_Large_01_5 = new Entity()
log_Large_01_5.setParent(scene)
log_Large_01_5.addComponentOrReplace(gltfShape_23)
const transform_41 = new Transform({
  position: new Vector3(8, 0, 12),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_5.addComponentOrReplace(transform_41)
engine.addEntity(log_Large_01_5)

const log_Large_01_6 = new Entity()
log_Large_01_6.setParent(scene)
log_Large_01_6.addComponentOrReplace(gltfShape_23)
const transform_42 = new Transform({
  position: new Vector3(24, 0, 16),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_6.addComponentOrReplace(transform_42)
engine.addEntity(log_Large_01_6)

const log_Large_01_7 = new Entity()
log_Large_01_7.setParent(scene)
log_Large_01_7.addComponentOrReplace(gltfShape_23)
const transform_43 = new Transform({
  position: new Vector3(24, 0, 21),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_7.addComponentOrReplace(transform_43)
engine.addEntity(log_Large_01_7)

const log_Large_01_8 = new Entity()
log_Large_01_8.setParent(scene)
log_Large_01_8.addComponentOrReplace(gltfShape_23)
const transform_44 = new Transform({
  position: new Vector3(8, 0, 22),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_8.addComponentOrReplace(transform_44)
engine.addEntity(log_Large_01_8)

const log_Large_01_9 = new Entity()
log_Large_01_9.setParent(scene)
log_Large_01_9.addComponentOrReplace(gltfShape_23)
const transform_45 = new Transform({
  position: new Vector3(11.5, 0, 25),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_9.addComponentOrReplace(transform_45)
engine.addEntity(log_Large_01_9)

const log_Large_01_10 = new Entity()
log_Large_01_10.setParent(scene)
log_Large_01_10.addComponentOrReplace(gltfShape_23)
const transform_46 = new Transform({
  position: new Vector3(20, 0, 25),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_10.addComponentOrReplace(transform_46)
engine.addEntity(log_Large_01_10)

const log_Large_01_11 = new Entity()
log_Large_01_11.setParent(scene)
log_Large_01_11.addComponentOrReplace(gltfShape_23)
const transform_47 = new Transform({
  position: new Vector3(8, 0, 17),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(1, 1, 1)
})
log_Large_01_11.addComponentOrReplace(transform_47)
engine.addEntity(log_Large_01_11)

const log_Medium_01 = new Entity()
log_Medium_01.setParent(scene)
const gltfShape_24 = new GLTFShape('models/Log_Medium_01/Log_Medium_01.glb')
log_Medium_01.addComponentOrReplace(gltfShape_24)
const transform_48 = new Transform({
  position: new Vector3(10, 0, 24),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
log_Medium_01.addComponentOrReplace(transform_48)
engine.addEntity(log_Medium_01)

const rock_01 = new Entity()
rock_01.setParent(scene)
const gltfShape_25 = new GLTFShape('models/Rock_01/Rock_01.glb')
rock_01.addComponentOrReplace(gltfShape_25)
const transform_49 = new Transform({
  position: new Vector3(24.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
rock_01.addComponentOrReplace(transform_49)
engine.addEntity(rock_01)

const house = new Entity()
house.setParent(scene)
/*
Model texture by Nghiem Xuan Cuong (CGTraining) Comcept by Assassin’s - Liberation
https://sketchfab.com/3d-models/house-6a834c1320c245ac87d16784683aeeec
*/
const houseShape = new GLTFShape('models/HouseSwamp/HouseSwamp.glb')
house.addComponentOrReplace(houseShape)
const houseTransform = new Transform({
  position: new Vector3(17, 0, 19),
  rotation: Quaternion.Euler(0, -90, 0),
  scale: new Vector3(0.8, 0.8, 0.8)
})
house.addComponentOrReplace(houseTransform)
engine.addEntity(house)

/* Builder HUB
import {BuilderHUD} from './modules/BuilderHUD'
const hud:BuilderHUD =  new BuilderHUD()
hud.attachToEntity(house)
hud.setDefaultParent(scene)
*/
